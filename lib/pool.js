'use strict';

const Abstract = require('@scola/transport-pool');
const Error = require('@scola/error');

class DealerPool extends Abstract.Pool {
  constructor(queue) {
    super();

    this.queue = queue.setProcessor(this.processQueue.bind(this));
    this.pointer = 0;

    this.bindQueueHandlers();
  }

  close() {
    this.unbindQueueHandlers();
    return super.close();
  }

  send(message) {
    this.queue.add(message);
    return super.send(message);
  }

  cancel(message) {
    this.queue.delete(message);
    return super.cancel(message);
  }

  bindQueueHandlers() {
    this.bindListener('error', this.queue, this.handleQueueError);
  }

  unbindQueueHandlers() {
    this.unbindListener('error', this.queue, this.handleQueueError);
  }

  processQueue(message) {
    this.emit('debug', this, 'processQueue', message);

    return new Promise((resolve, reject) => {
      const connection = this.findConnection(this.pointer);

      if (connection) {
        connection.send(message);
        return resolve();
      }

      return reject(new Error('transport_message_not_sent', {
        detail: {
          message
        }
      }));
    });
  }

  handleQueueError(error) {
    this.emit('error', error);
  }

  findConnection(start) {
    this.emit('debug', this, 'findConnection', start, this.pointer);

    const connection = [...this.connections][this.pointer];

    this.pointer =
      this.pointer < this.connections.size - 1 ?
      this.pointer + 1 :
      0;

    if (connection && connection.canSend()) {
      return connection;
    } else if (this.pointer === start) {
      return null;
    }

    return this.findConnection(start);
  }
}

module.exports = DealerPool;
