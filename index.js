'use strict';

const DI = require('@scola/di');
const Async = require('@scola/async');
const Pool = require('./lib/pool');

class Module extends DI.Module {
  configure() {
    this.inject(Pool).with(
      this.instance(Async.Queue)
    );
  }
}

module.exports = {
  Module,
  Pool
};
